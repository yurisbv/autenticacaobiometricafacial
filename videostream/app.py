from flask import Flask, render_template, Response
from flask_socketio import SocketIO, emit
from threading import Lock
import os
import time
import cv2
import numpy as np
import base64

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
#thread = None
#thread_lock = Lock()


classificadorFace = cv2.CascadeClassifier('frontalface.xml')
# classificadorNose = cv2.CascadeClassifier(path+'nose-25x15.xml')
# classificadorLeftEye = cv2.CascadeClassifier(path+'left-eye-18x12.xml')
# classificadorRightEye = cv2.CascadeClassifier(path+'right-eye-18x12.xml')
#classificadorEye = cv2.CascadeClassifier(path+'haarcascade_eye.xml')
# classificadorMouth = cv2.CascadeClassifier(path+'mouth-25x15.xml')


#def background_thread():
#    while True:
#        socketio.sleep(1/8)
#        image = open(os.getcwd() + '/face_and_eye.jpeg', 'rb')
#        print(image)
#        image_read = image.read()
#        image_64_encode = base64.b64encode(image_read)
#        socketio.emit('image_return', {'data': 'data:image/jpeg;base64,{}'.format(image_64_encode.decode())})


@app.route('/')
def index():
    return render_template('index.html')


def data_uri_to_cv2_img(uri):

    encoded_data = uri.split(',')[1]
    nparr = np.fromstring(base64.decodestring(bytes(encoded_data, 'utf-8')), dtype=np.uint8)

    ##CV2
    img = cv2.imdecode(nparr, 1)
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    face = classificadorFace.detectMultiScale(gray_image, scaleFactor=1.1, minNeighbors=9, minSize=(120, 120))
    for (x, y, w, h) in face:
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 3)
            #roi = img[y:y + h, x:x + w]
    #     #
    # mouth = classificadorMouth.detectMultiScale(gray_image, scaleFactor=1.1, minNeighbors=9, minSize=(25, 15))
    # for (mx, my, mw, mh) in mouth:
    #     cv2.rectangle(img, (mx, my), (mx + mw, my + mh), (255, 255, 0), 4)

    # mouth = classificadorMouth.detectMultiScale(gray_image, scaleFactor=1.1, minNeighbors=9, minSize=(15, 25))
    # for (x, y, h, l) in mouth:
    #     cv2.rectangle(img, (x, y), (x + h, y + l), (255, 0, 255), 1)

    # face = classificadorFace.detectMultiScale(gray_image, 1.3, 5)
    # for (x, y, w, h) in face:
    #     cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    #     roi_gray = gray_image[y:y + h, x:x + w]
    #     roi_color = img[y:y + h, x:x + w]
    #     eyes = classificadorEye.detectMultiScale(roi_gray)
    #     for (ex, ey, ew, eh) in eyes:
    #         cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    cv2.imwrite(os.getcwd()+'/face_and_eye.jpeg', img)

    ##Preparando para retornar
    image = open(os.getcwd()+'/face_and_eye.jpeg', 'rb')

    image_read = image.read()
    image_64_encode = base64.b64encode(image_read)
    return image_64_encode


@socketio.on('stream_server')
def handle_stream_server_event(frame):
    gray = data_uri_to_cv2_img(frame['data'])
    # emit('image_return', {'data': 'data:image/jpeg;base64,{}'.format(gray.decode())})
    # global thread
    # with thread_lock:
    #     if thread is None:
    #         thread = socketio.start_background_task(target=background_thread)


def gen():
    while True:
        # frame = camera.get_frame()
        image = open(os.getcwd() + '/face_and_eye.jpeg', 'rb')
        # image_read = image.read()
        #image_64_encode = base64.b64encode(image_read)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + image + b'\r\n')


# @app.route('/video_feed')
# def video_feed():
#     return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    socketio.run(app, '0.0.0.0', 9090)


#https://code.tutsplus.com/tutorials/base64-encoding-and-decoding-using-python--cms-25588
#https://stackoverflow.com/questions/33754935/read-a-base-64-encoded-image-from-memory-using-opencv-python-library
#https://stackoverflow.com/questions/23187916/base64-type-err-typeerror-expected-bytes-not-str
#https://stackoverflow.com/questions/8908287/why-do-i-need-b-to-encode-a-python-string-with-base64
