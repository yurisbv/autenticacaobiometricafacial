from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from threading import Lock
import os
import cv2
import numpy as np
import base64

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
thread = None
thread_lock = Lock()


classificadorFace = cv2.CascadeClassifier('frontalface.xml')


def background_thread():
    while True:
        socketio.sleep(1/10)
        image = open(os.getcwd() + '/face_and_eye.jpeg', 'rb')
        image_read = image.read()
        image_64_encode = base64.b64encode(image_read)
        socketio.emit('image_return', {'data': 'data:image/jpeg;base64,{}'.format(image_64_encode.decode())})


@app.route('/')
def index():
    return render_template('index.html')


def data_uri_to_cv2_img(uri):
    encoded_data = uri.split(',')[1]
    nparr = np.fromstring(base64.decodestring(bytes(encoded_data, 'utf-8')), dtype=np.uint8)
    ##CV2
    img = cv2.imdecode(nparr, 1)
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    face = classificadorFace.detectMultiScale(gray_image, scaleFactor=1.1, minNeighbors=9, minSize=(140, 140))
    for (x, y, w, h) in face:
        cv2.rectangle(img, (x, y), (x + w, y + h), (50, 255, 100), 3)
        cv2.line(img, (x-5, y), (x + w - 5, y), (0, 255, 0), 30)
        cv2.putText(img, "Yuri Brandao", (x + 2, y + 10), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 0), 2)
    cv2.imwrite(os.getcwd()+'/face_and_eye.jpeg', img)


@socketio.on('stream_server')
def handle_stream_server_event(frame):
    data_uri_to_cv2_img(frame['data'])    
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread)


if __name__ == '__main__':
    socketio.run(app, '0.0.0.0', 9090)
    
