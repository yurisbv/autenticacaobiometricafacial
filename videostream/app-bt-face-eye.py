from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from threading import Lock
import os
import time
import cv2
import numpy as np
import base64

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
thread = None
thread_lock = Lock()

path = '/home/yurisbv/Documentos/TCC/autenticacaobiometricafacial/videostream/'
classificadorFace = cv2.CascadeClassifier(path+'frontalface.xml')
classificadorEye = cv2.CascadeClassifier(path+'haarcascade_eye.xml')



def background_thread():
    while True:
        socketio.sleep(1/12)
        image = open(os.getcwd() + '/face_and_eye.jpeg', 'rb')
        image_read = image.read()
        image_64_encode = base64.b64encode(image_read)
        socketio.emit('image_return', {'data': 'data:image/jpeg;base64,{}'.format(image_64_encode.decode())})


@app.route('/')
def index():
    return render_template('index.html')


def data_uri_to_cv2_img(uri):

    encoded_data = uri.split(',')[1]
    nparr = np.fromstring(base64.decodestring(bytes(encoded_data, 'utf-8')), dtype=np.uint8)

    ##CV2
    img = cv2.imdecode(nparr, 1)
    gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    face = classificadorFace.detectMultiScale(gray_image, 1.3, 5)
    for (x, y, w, h) in face:
        cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
        roi_gray = gray_image[y:y + h, x:x + w]
        roi_color = img[y:y + h, x:x + w]
        eyes = classificadorEye.detectMultiScale(roi_gray)
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
    cv2.imwrite(os.getcwd() + '/face_and_eye.jpeg', img)


@socketio.on('stream_server')
def handle_stream_server_event(frame):
    data_uri_to_cv2_img(frame['data'])
    global thread
    with thread_lock:
        if thread is None:
            thread = socketio.start_background_task(target=background_thread)


if __name__ == '__main__':
    socketio.run(app, '0.0.0.0', 9090)


#https://code.tutsplus.com/tutorials/base64-encoding-and-decoding-using-python--cms-25588
#https://stackoverflow.com/questions/33754935/read-a-base-64-encoded-image-from-memory-using-opencv-python-library
#https://stackoverflow.com/questions/23187916/base64-type-err-typeerror-expected-bytes-not-str
#https://stackoverflow.com/questions/8908287/why-do-i-need-b-to-encode-a-python-string-with-base64
