import cv2


classificador = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')

imagem = cv2.imread('pessoas/pessoas4.jpg')

imagemCinza = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)

faces = classificador.detectMultiScale(imagemCinza, scaleFactor=1.1, minNeighbors=9, minSize=(35,35))

print(len(faces))

for(x, y, h, l) in faces:
    cv2.rectangle(imagem, (x , y), (x + h, y + l), (0,0,255), 3)

cv2.imshow('foda', imagem)

cv2.waitKey()