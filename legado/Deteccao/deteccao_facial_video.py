import cv2

import os


def validar_pasta(nome):
    if os.path.isdir("./" + nome):
        print("nome já existente")
        return False

    return True


def criar_pasta(nome):
    os.makedirs('./' + nome)

def mensagem_sucesso_pasta(nome):
    print("Pasta" + "/"+nome+" criada com sucesso!")

def limpar_registros_video(video):
    video.release()
    cv2.destroyAllWindows()

def identificar_faces():
    image_faces = []

    qtd_faces_detectadas=0

    classificadorFaces = cv2.CascadeClassifier('../cascades/haarcascade_frontalface_default.xml')

    video = cv2.VideoCapture(0)

    while True:
        imagem, frame = video.read()

        frame_cinza = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces_detectadas = classificadorFaces.detectMultiScale(frame_cinza, minNeighbors=5, scaleFactor=1.5, minSize=(70, 70))

        print(faces_detectadas)
        if(len(faces_detectadas)==1):
            qtd_faces_detectadas=qtd_faces_detectadas+1
            print(qtd_faces_detectadas)

        for x, y, l, a in faces_detectadas:
            cv2.rectangle(frame, (x, y), (x + l, y + a), (0, 0, 255), 2)
            image_faces.append(frame_cinza[y:(y + a), x:(x + l)])

        cv2.imshow('video', frame)

        if len(image_faces)==100 or cv2.waitKey(1)==ord('f'):
            nome = input("Escreva o seu nome:\n");

            while validar_pasta(nome)==False:
                nome = input("Escreva o seu nome:\n");

            criar_pasta(nome)

            for i, face in enumerate(image_faces):
                cv2.imwrite("./"+nome+"/"+"face-" + str(i) + ".jpg", face)
            break;

    mensagem_sucesso_pasta(nome)
    limpar_registros_video(video)

identificar_faces()
