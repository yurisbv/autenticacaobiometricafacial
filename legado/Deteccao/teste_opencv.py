import cv2

print(cv2.__version__)

mustang = cv2.imread('mustang.jpg')
mustang_old=cv2.cvtColor(mustang, cv2.COLOR_BGR2GRAY)
cv2.imshow('Roy Mustang', mustang)
cv2.imshow('Old Roy Mustang', mustang_old)
cv2.waitKey()