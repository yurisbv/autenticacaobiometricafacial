import cv2

classificador_face = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')

classificador_olho = cv2.CascadeClassifier('cascades/haarcascade_eye.xml')

imagem = cv2.imread('pessoas/pessoas4.jpg')

imagem_cinza = cv2.cvtColor(imagem, cv2.COLOR_BGR2GRAY)

faces_detectadas = classificador_face.detectMultiScale(imagem_cinza)

if len(faces_detectadas) == 0:
    olhos_detecados = classificador_olho.detectMultiScale(imagem_cinza)

    for(xo, yo, lo, ao) in olhos_detecados:
        cv2.rectangle(imagem, (xo, yo), (xo+lo, yo + ao), (0,0,255), 2)


for(x, y, a, l) in faces_detectadas:
    cv2.rectangle(imagem, (x, y), (x+l, y + a), (0,0,255), 2)
    area_selecionada = imagem[y:y+a, x:x+l]
    area_selecionada_cinza = cv2.cvtColor(area_selecionada, cv2.COLOR_BGR2GRAY)
    olhos_detecados = classificador_olho.detectMultiScale(area_selecionada_cinza)
    for(xo, yo, ao, lo) in olhos_detecados:
        cv2.rectangle(area_selecionada, (xo, yo), (xo+lo, yo+ao), (255,0,0), 1)


cv2.imshow('menina', imagem)

cv2.waitKey();