import cv2

import cv2.face

from PIL import Image

import os

import numpy as np

reconhecedor_eigenface = cv2.face.EigenFaceRecognizer_create()
reconhecedor_eigenface.read('classificadorEigenYale.yml')

reconhecedor_fisherface = cv2.face.FisherFaceRecognizer_create()
reconhecedor_fisherface.read('classificadorFisherYale.yml')

reconhecedor_lbph = cv2.face.LBPHFaceRecognizer_create()
reconhecedor_lbph.read('classificadorLbphYale.yml')

classificadorFaces = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')

faces=[]

total_acertos=0
total_imagens=0
percentual_acerto=0.0
total_confianca=0.0
confianca_media=0.0

caminhos = [os.path.join('yalefaces/teste', f) for f in os.listdir('yalefaces/teste')]
for j in caminhos:
    imagem_face = Image.open(j).convert('L')
    imagem_np = np.array(imagem_face, 'uint8')
    faces.append(imagem_np)

    faces_classificadas=classificadorFaces.detectMultiScale(imagem_np)

    for x,y,l,a in faces_classificadas:
        #cv2.rectangle(imagem_np, (x,y), (x+l, y+a), (0,0,255), 2)
        #cv2.imshow("imagem", imagem_np)
        #cv2.waitKey(500)
        idreconhecido, confianca = reconhecedor_lbph.predict(imagem_np)
        idatual = int(os.path.split(j)[1].split('.')[0].replace("subject", ''))
        if idreconhecido==idatual:
            total_acertos+= 1
            total_confianca+=confianca

        print(str(idreconhecido) + ' - ' + str(idatual) + ' - ' + str(confianca))
        total_imagens+=1

percentual_acerto = (total_acertos*100)/total_imagens
confianca_media = total_confianca/total_imagens

print('Percentual de acerto: ' + str(percentual_acerto) + ' - ' + 'Confiança Média: ' + str(confianca_media))