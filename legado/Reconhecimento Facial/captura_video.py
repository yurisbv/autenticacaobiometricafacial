import cv2

import os

import numpy as np

def validar_pasta(nome):
    if os.path.isdir("./" + nome):
        print("nome já existente")
        return False

    return True


def criar_pasta(nome):
    os.makedirs('./fotos/' + nome)

def mensagem_sucesso_pasta(nome):
    print("Pasta" + "/"+nome+" criada com sucesso!")

def limpar_registros_video(video):
    video.release()
    cv2.destroyAllWindows()

def identificar_faces():
    image_faces = []

    largura = 220

    altura = 220

    contador=0

    classificadorFaces = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')

    nome=''

    while validar_pasta(nome) == False:
        nome = input("Escreva o seu nome:\n");
    criar_pasta(nome)

    video = cv2.VideoCapture(0)

    while True:
        imagem, frame = video.read()

        frame_cinza = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        faces_detectadas = classificadorFaces.detectMultiScale(frame_cinza, minSize=(70, 70))

        for x, y, l, a in faces_detectadas:
            cv2.rectangle(frame, (x, y), (x + l, y + a), (0, 0, 255), 2)

        if cv2.waitKey(1)==ord('f'):
            if np.average(frame_cinza) > 110:
                image_faces.append(cv2.resize(frame_cinza[y:(y + a), x:(x + l)], (largura, altura)))
                cv2.imwrite("./fotos/" + nome + "/" + "face-" + str(contador) + ".jpg", image_faces[contador])
                contador+=1

        cv2.imshow('video', frame)

        if len(image_faces)==25:
            break;

    mensagem_sucesso_pasta(nome)
    limpar_registros_video(video)

identificar_faces()
