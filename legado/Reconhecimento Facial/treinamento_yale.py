import cv2

import cv2.face

from PIL import Image

import os

import numpy as np


eigenface = cv2.face.EigenFaceRecognizer_create()

fisherface = cv2.face.FisherFaceRecognizer_create()

lbph = cv2.face.LBPHFaceRecognizer_create()

faces=[]

ids=[]

def get_imagem():
    id=0;
    caminhos = [os.path.join('yalefaces/treinamento', f) for f in os.listdir('yalefaces/treinamento')]
    for j in caminhos:
        imagem_face=Image.open(j).convert('L')
        imagem_np=np.array(imagem_face, 'uint8')
        id=int(os.path.split(j)[1].split('.')[0].replace("subject", ''))
        ids.append(id)
        faces.append(imagem_np)

    return np.array(ids), faces

ids, faces = get_imagem()

print(ids)

eigenface.train(faces, ids)
eigenface.write('classificadorEigenYale.yml')

fisherface.train(faces, ids)
fisherface.write('classificadorFisherYale.yml')

lbph.train(faces, ids)
lbph.write('classificadorLbphYale.yml')