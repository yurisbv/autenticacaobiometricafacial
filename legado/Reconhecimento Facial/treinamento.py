import cv2

import cv2.face

import os

import numpy as np


eigenface = cv2.face.EigenFaceRecognizer_create()

fisherface = cv2.face.FisherFaceRecognizer_create(threshold=1000)

lbph = cv2.face.LBPHFaceRecognizer_create(threshold=50)
lbph.read('classificadorLbph.yml')

faces=[]

ids=[]

def get_imagem():
    id=0;
    caminhos = [os.path.join('fotos', f) for f in os.listdir('fotos')]
    for j in caminhos:
        subcaminho = [os.path.join(j, p) for p in os.listdir(j)]
        #print(subcaminho)
        #print("\n")
        id=2
        for imagem in subcaminho:
            imagem_face = cv2.cvtColor(cv2.imread(imagem), cv2.COLOR_BGR2GRAY)
            nome=j[6:]
            print(id)
            print(nome)
            #print(imagem_face)
            #nomes.append(nome)
            ids.append(id)
            faces.append(imagem_face)
            #cv2.imshow("imagem", imagem_face)
            #cv2.waitKey(ord('n'))
    return np.array(ids), faces

ids, faces = get_imagem()

print(ids)

lbph.update(faces, ids)
lbph.write('classificadorLbph.yml')