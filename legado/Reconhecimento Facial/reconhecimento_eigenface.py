import cv2

camera = cv2.VideoCapture(0)

classificador = cv2.CascadeClassifier('cascades/haarcascade_frontalface_default.xml')

altura = 220

largura = 220

font = cv2.FONT_HERSHEY_COMPLEX_SMALL

reconhecedor = cv2.face.EigenFaceRecognizer_create()
reconhecedor.read('classificadorEigen.yml')

while True:

    imagem, frame = camera.read()

    imagem_cinza = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces_classificadas = classificador.detectMultiScale(imagem_cinza)

    for x, y, l, a in faces_classificadas:
        imagem_face = cv2.resize(imagem_cinza[y:(y+a), x:(x+l)], (largura, altura))

        cv2.rectangle(frame, (x,y), (x+l, y+a), (0,0,255), 2)

        id, confianca = reconhecedor.predict(imagem_face)

        cv2.putText(frame, str(id), (x,y + (a+30)), font, 2, (0,0,255))
        cv2.putText(frame, str(confianca), (x,y + (a+50)), font, 2, (0,0,255))

    cv2.imshow("face", frame)

    if cv2.waitKey(1) == ord('f'):
        break;

camera.release()
cv2.destroyAllWindows()

