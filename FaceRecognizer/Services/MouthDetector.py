import cv2

class MouthDetector():
    _haarCascadeMouth = 'Dependencies/mouth.xml'
    _grayMouth = None

    def __init__(self):
        self._classifierMouth = cv2.CascadeClassifier(self._haarCascadeMouth)

    def validation_mouth(self, face):
        self.detect_mouth(face)
        mouth=self.get_mouth(face)
        if(mouth!=()):
            return True
        return False

    def detect_mouth(self, face):
        self._grayMouth=self._classifierMouth.detectMultiScale(face)

    def get_mouth(self, face):
        for mx, my, ml, ma in self._grayMouth:
            mouth=face[my:(my + ma), mx:(mx + ml)]
            return mouth