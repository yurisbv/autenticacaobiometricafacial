import cv2

class EyesDetector():
    _haarCascadeLeftEye = 'Dependencies/lefteye.xml'
    _haarCascadeRightEye = 'Dependencies/righteye.xml'
    _grayLeftEye = None
    _grayRightEye = None

    def __init__(self):
        self._classifierLeftEye = cv2.CascadeClassifier(self._haarCascadeLeftEye)
        self._classifierRightEye = cv2.CascadeClassifier(self._haarCascadeRightEye)

    def validation_eyes(self, face):
        self.detect_left_eye(face)
        self.detect_right_eye(face)
        left_eye=self.get_left_eye(face)
        right_eye=self.get_right_eye(face)
        if(left_eye!=() and right_eye!=()):
            return True
        return False

    def detect_left_eye(self, face):
        self._grayLeftEye=self._classifierLeftEye.detectMultiScale(face)

    def detect_right_eye(self, face):
        self._grayRightEye=self._classifierRightEye.detectMultiScale(face)

    def get_left_eye(self, face):
        for ox, oy, ol, oa in self._grayLeftEye:
            left_eye=face[oy:(oy + oa), ox:(ox + ol)]
            return left_eye

    def get_right_eye(self, face):
        for ox, oy, ol, oa in self._grayRightEye:
            right_eye=face[oy:(oy + oa), ox:(ox + ol)]
            return right_eye