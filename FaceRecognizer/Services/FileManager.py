import codecs
import os
import cv2
import numpy as np
import re

class FileManager(object):

    def create_directory(self, user):
        if os.path.isdir('users'):
           os.makedirs( 'users/' + str(user.get_id()))
        else:
            os.makedirs('users')
            os.makedirs('users/' + str(user.get_id()))

    def delete_directory(self, id):
        self.clear_photos(id)
        os.remove('users/' + str(id) + '/' + str(id) + '.txt')
        os.removedirs('users/' + str(id))
        self.remove_user_in_users_text(id)

    def is_valid_directory(self, user):
        if os.path.isdir('users/' + str(user.get_id())):
            return False
        return True

    def is_valid_yml(self):
        path = [os.path.join('Dependencies', f) for f in os.listdir('Dependencies')]
        for yml in path:
            if 'yml' in yml:
                return True

        return False

    def generate_user_info_text(self, id_user, name, create_at_user, last_change=''):
        with codecs.open( 'users/' + str(id_user) + '/' + str(id_user) + '.txt', 'w', encoding='utf-8') as file:
            text = str(id_user) + '\n' + \
                   name + '\n' + \
                   str(create_at_user) + '\n' + \
                   str(last_change)
            file.write(text)
            file.close()


    def generate_all_users_text(self, id_user):
        try:
            self.update_all_users_txt(id_user)
        except IOError:
            self.write_all_users_txt(id_user)

    def update_all_users_txt(self, id_user):
        file = open('users/users.txt', 'r')
        text = file.readlines()
        text.append(str(id_user) + '\n')
        file = open('users/users.txt', 'w')
        file.writelines(text)
        file.close()

    def write_all_users_txt(self, id_user):
        file = open('users/users.txt', 'w')
        text = str(id_user) + '\n'
        file.write(text)
        file.close()

    def remove_user_in_users_text(self, id_user):
        try:
            file = open('users/users.txt', 'r')
            text = file.readlines()
            new_text = self.rewrite_users_text(id_user, text, file)
            file = open('users/users.txt', 'w')
            file.writelines(new_text)
        except IOError:
            print('no file')
            raise

    def rewrite_users_text(self, id_user, text, file):
        new_text = file.readlines()
        new_text.clear()
        for line in text:
            if (line == str(id_user) + '\n'):
                continue
            new_text.append(line)
        return new_text

    def read_user_info_text(self, id_user):
        text=[]
        with codecs.open('users/' + str(id_user) + '/' + str(id_user) + '.txt', 'r', encoding='utf-8') as file:
            for line in file.readlines():
               text.append(line.replace('\n', ''))
            file.close()
            return text

    def read_all_users_text(self):
        file = open('users/users.txt', 'r')
        text = file.readlines()
        file.close()
        return text

    def save_image(self, image_faces, user):
        for i, face in enumerate(image_faces):
            cv2.imwrite("users/" + str(user.get_id()) + "/" + str(i) + ".jpg", face)

    def clear_photos(self, id):
        userPath = [os.path.join('users/'+str(id), p) for p in os.listdir('users/'+str(id))]
        for image in userPath:
            if ('.txt' in image):
                continue
            os.remove(image)

    def get_ids_faces(self):
        ids=[]
        faces=[]
        usersPath = [os.path.join('users', f) for f in os.listdir('users')]

        for user in usersPath:
            if('.txt' in user):
                continue
            userPath = [os.path.join(user, p) for p in os.listdir(user)]
            id=self.generate_int_id(user)
            for image in userPath:
                if('.txt' in image):
                    continue
                image_face = cv2.cvtColor(cv2.imread(image), cv2.COLOR_BGR2GRAY)
                ids.append(id)
                faces.append(image_face)

        return np.array(ids), faces

    def generate_int_id(self, user):
        filtro = re.compile('([0-9]+)')
        string_id = filtro.findall(user)
        id=''.join(string_id)
        return int(id)