import cv2

class NoseDetector():
    _haarCascadeNose = 'Dependencies/nose.xml'
    _grayNose = None

    def __init__(self):
        self._classifierNose = cv2.CascadeClassifier(self._haarCascadeNose)

    def validation_nose(self, face):
        self.detect_nose(face)
        nose=self.get_nose(face)
        if(nose!=()):
            return True
        return False

    def detect_nose(self, face):
        self._grayNose=self._classifierNose.detectMultiScale(face)

    def get_nose(self, face):
        for mx, my, ml, ma in self._grayNose:
            nose=face[my:(my + ma), mx:(mx + ml)]
            return nose