import cv2

import cv2.face

from FaceRecognizer.Services.FaceDetector import FaceDetector
from FaceRecognizer.Services.FileManager import FileManager


class FaceRecognizer(object):
    _lbphClassifier=None
    _fileManager = FileManager()
    _ids=None
    _threshold_limit = 100
    _id_final = 0
    _faces=[]

    def __init__(self, radius=8, neightbours=8, grade_x=7, grade_y=7, threshold=100):
        try:
            self._lbphClassifier=cv2.face.LBPHFaceRecognizer_create(radius,neightbours,grade_x,grade_y, threshold)
            self._threshold_limit=threshold
        except:
            print('error value')

    def trainining(self):
        self._ids, self._faces=self._fileManager.get_ids_faces()

        if(self._fileManager.is_valid_yml()==True):
            self.update_classifier_lbph()
        else:
            self.train_classifier_lbph()

    def update_classifier_lbph(self):
        self._lbphClassifier.read('Dependencies/classifierLbph.yml')
        self._lbphClassifier.update(self._faces, self._ids)
        self._lbphClassifier.write('Dependencies/classifierLbph.yml')

    def train_classifier_lbph(self):
        self._lbphClassifier.train(self._faces, self._ids)
        self._lbphClassifier.write('Dependencies/classifierLbph.yml')

    def recognizer(self, video, max_faces=50, duration=None):
        try:
            self._faceDetection = FaceDetector(max_faces, duration)
            image_faces=self._faceDetection.detect(video)
            if(image_faces==False):
                return
            return self.look_for_user(image_faces)
        except:
            print('erro ao executar método de reconhecimento')
            return


    def look_for_user(self, image_faces):
        # number_id_ok = 0
        try:
            self._lbphClassifier.read('Dependencies/classifierLbph.yml')
            self.identify_user(image_faces)
        except:
            return

    def identify_user(self, image_faces):
        self.process_face(image_faces)
        self.get_id_final()

    def get_id_final(self):
        if (self._id_final > 0):
            print(self._id_final)
            return self._id_final
        print('usuário não encontrado')

    def process_face(self, image_faces):
        for face in image_faces:
            id, confianca = self._lbphClassifier.predict(face)
            print(confianca)
            self.validate_min_confianca_and_id_final(id, confianca)

    def validate_min_confianca_and_id_final(self, id, confianca):
        if (id > 0):
            if (self._threshold_limit > confianca):
                self.set_min_confianca(confianca)
                self.set_id_final(id)

    def set_id_final(self, id):
        self._id_final=id

    def set_min_confianca(self, confianca):
       self._threshold_limit = confianca

    # def validate_id(self, number_id_ok, max_faces):
    #     value=(number_id_ok/max_faces) * 100
    #     print(value)
    #     if(value > 50):
    #         return True
    #     return False