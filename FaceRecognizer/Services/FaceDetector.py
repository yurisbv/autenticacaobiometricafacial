# from FaceRecognizer.MetaClass.MetaSingleton import MetaSingleton
import cv2
import os
import datetime
import numpy as np

from moviepy.editor import VideoFileClip

from FaceRecognizer.Entities.User import User
from FaceRecognizer.Services.EyesDetector import EyesDetector
from FaceRecognizer.Services.FileManager import FileManager
from FaceRecognizer.Services.MouthDetector import MouthDetector
from FaceRecognizer.Services.NoseDetector import NoseDetector


class FaceDetector(object):
    _haarCascade = 'Dependencies/frontalface.xml'
    _maxfaces = 50
    _minfaces = 10
    _duration=None
    _video = None
    _classifier = None
    _user = User()
    _file_manager = FileManager()
    _eyeDetector = EyesDetector()
    _mouthDetector = MouthDetector()
    _noseDetector = NoseDetector()
    _faces=[]

    def __init__(self, maxfaces=50, minfaces=15, duration=None):
        self._classifier = cv2.CascadeClassifier(self._haarCascade)
        self.set_min_faces(minfaces)
        self.set_max_faces(maxfaces)

    def detect(self, video, user=None, faceRecognizer=None):
        try:
            image_faces = []
            self.start_detection(video, user)
            while True:
                try:
                    image, frame = self._video.read()
                    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                except:
                    print('Vídeo inválido')
                    return False
                detected_faces = self.detect_faces(frame_gray)

                if(detected_faces!=()):
                    image_faces=self.add_image_faces(detected_faces, image_faces, frame_gray)

                if len(image_faces) == self._maxfaces:
                    if(user!=None):
                        self.validate_image_faces_to_create(faceRecognizer, image_faces, user)
                        break
                    else:
                        image_faces=self.validate_image_faces_to_recognizer(image_faces)
                        return image_faces

            self.stop_video_process()
        except:
            print('Erro ao executar o método de detecção')

    def start_detection(self, video, user):
        if (self.validate_number_of_faces(self._minfaces, self._maxfaces) == False):
            print('min>max')
            return
        else:
            self._user = user
            self.set_video(video)

    def validate_image_faces_to_create(self, faceRecognizer, image_faces, user):
        if (faceRecognizer is not None and faceRecognizer.look_for_user(image_faces) is not None):
            print('Imagem já registrada')
            return
        self.prepare_to_save_faces(image_faces, user)


    def validate_image_faces_to_recognizer(self, image_faces):
        image_faces = self.validation_face(image_faces)
        self.stop_video_process()
        if (len(image_faces) <= self._minfaces):
            print('faces inválidas')
            return

        return image_faces

    def detect_faces(self, frame_gray):
        detected_face = self._classifier.detectMultiScale(frame_gray, scaleFactor=1.1, minNeighbors=9, minSize=(140, 140))
        return detected_face

    def transform_detected_faces_in_face(self, frame_gray, detected_faces):
        for x, y, l, a in detected_faces:
            return frame_gray[y:(y + a), x:(x + l)]

    def validation_face_components(self, face):
        if(self._eyeDetector.validation_eyes(face) == True and self._mouthDetector.validation_mouth(face)==True and self._noseDetector.validation_nose(face)==True):
            return True
        else:
            return False

    def validation_face(self, image_faces):
        try:
            count=0
            index=0
            new_image_faces=[]
            for face1 in self._faces:
                for face2 in self._faces:
                    if(np.all(face1==face2)):
                        count+=1
                if(count==1):
                    new_image_faces.append(image_faces[index])
                    index += 1
                count=0
        except:
            print('erro ao validar face' + ' ' + str(index))

        return new_image_faces

    def add_image_faces(self, detected_faces, image_faces, frame_gray):
        if (self.validation_face_components(self.transform_detected_faces_in_face(frame_gray, detected_faces))):
            self._faces.append(detected_faces)
            image_faces.append(self.transform_detected_faces_in_face(frame_gray, detected_faces))

        return image_faces

    def prepare_to_save_faces(self, image_faces, user):
        image_faces = self.validation_face(image_faces)
        if (len(image_faces) >= self._minfaces):
            self._file_manager.clear_photos(user.get_id())
            self._file_manager.save_image(image_faces, user)
            print('sucesso ao salvar faces')
        else:
            print('fracasso ao salvar faces')

    def validate_number_of_faces(self, minfaces, maxfaces):
        if(minfaces > maxfaces):
            return False
        return True

    def set_video(self, video):
        videoClip=VideoFileClip(video)
        if(self._duration is None or videoClip.duration<=self._duration):
            self._video = cv2.VideoCapture(video)
        print(videoClip.duration)

    def stop_video_process(self):
        self._video.release()
        cv2.destroyAllWindows()

    def set_min_faces(self, minfaces):
        if minfaces and isinstance(minfaces, int):
            self._minfaces=minfaces

    def set_max_faces(self, maxfaces):
        if maxfaces and isinstance(maxfaces, int):
            self._maxfaces = maxfaces

    def set_duration(self, duration):
        if duration is not None and duration and isinstance(duration, float):
            self._duration=duration