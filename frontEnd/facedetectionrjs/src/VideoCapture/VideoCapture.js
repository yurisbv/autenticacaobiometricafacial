import React, { Component } from 'react'
import './build/tracking'
import './build/data/face'

export default class VideoCapture extends Component {

  ComponentDidMount(){
    var video = document.getElementById('video');
    var canvas = document.getElementById('canvas');
    var canvas2 = document.getElementById('canvas2');
    var context = canvas.getContext('2d');
    var context2 = canvas2.getContext('2d');
    //Usa viola-jones HAAR cascade gerado pelo OPENCV
    var tracker = new tracking.ObjectTracker('face');  //https://trackingjs.com/api/tracking.ObjectTracker.html
        tracker.setInitialScale(2);
        tracker.setStepSize(1.5);
        tracker.setEdgesDensity(0.3);

    tracking.track('#video', tracker, { camera: true });

    tracker.on('track', function(event) {
      context.clearRect(0, 0, canvas.width, canvas.height);
      context2.clearRect(0, 0, canvas2.width, canvas2.height);

      event.data.forEach(function(rect) {
        console.log("achou uma face");
        var img = document.createElement("img");
        context.strokeStyle = '#a64ceb';
        context.strokeRect(rect.x, rect.y, rect.width, rect.height);
        context2.drawImage(video, rect.x+75, rect.y+75, canvas.width-40, canvas.height-40, 0,0,120,120);
        setTimeout(()=>{
            img.src = canvas2.toDataURL('image/png');
            document.getElementById("images").appendChild(img);
        },100);
      });
    });
  }

  render() {
    return (
      <div>
          <video id="video" width="320" height="240" preload="true" autoPlay loop muted></video>
          <canvas id="canvas" width="320" height="240"></canvas>
          <canvas id="canvas2" width="120" height="120"></canvas>
     </div>
    )
  }
}
