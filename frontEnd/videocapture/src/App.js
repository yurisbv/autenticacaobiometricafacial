import React, { Component } from 'react';
// import AudioExample from './AudioExample';
import VideoExample from './VideoExample';

export default class App extends Component {
	render() {
		return (
			<div>
				<h1>React Multimedia Capture Test</h1>
				<hr />

				<VideoExample />
			</div>
		);
	}
};