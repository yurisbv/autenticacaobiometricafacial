# from dependencies.face_recognizer.Services.UserManager import UserManager
# from dependencies.face_recognizer.Entities.User import User
from dependencies.face_recognizer.MetaClass.MetaSingleton import MetaSingleton
import codecs
import os


class FileManager(object, metaclass=MetaSingleton):

    def create_directory(self, user):
        if os.path.isdir('users'):
           os.makedirs('users/' + str(user.get_id()))
        else:
            os.makedirs('users')
            os.makedirs('users/' + str(user.get_id()))

    def delete_directory(self, id):
        os.remove('users/' + str(id) + '/' + str(id) + '.txt')
        os.removedirs('users/' + str(id))
        self.remove_user_in_users_text(id)

    def is_valid_directory(self, user):
        if os.path.isdir('users/' + str(user.get_id())):
            return False
        return True

    def generate_user_info_text(self, id_user, name, create_at_user, last_change=''):
        with codecs.open( 'users/' + str(id_user) + '/' + str(id_user) + '.txt', 'w', encoding='utf-8') as file:
            text = str(id_user) + '\n' + \
                   name + '\n' + \
                   str(create_at_user) + '\n' + \
                   str(last_change)
            file.write(text)
            file.close()


    def generate_all_users_text(self, id_user):
        try:
            file = open('users/users.txt', 'r')
            text = file.readlines()
            text.append(str(id_user) + '\n')
            file = open('users/users.txt', 'w')
            file.writelines(text)
            file.close()
        except IOError:
            file = open('users/users.txt', 'w')
            text = str(id_user)+ '\n'
            file.write(text)
            file.close()

    def remove_user_in_users_text(self, id_user):
        try:
            file = open('users/users.txt', 'r')
            text = file.readlines()
            new_text=file.readlines()
            new_text.clear()
            for line in text:
                if(line==str(id_user)+'\n'):
                    continue
                new_text.append(line)
            file = open('users/users.txt', 'w')
            file.writelines(new_text)
        except IOError:
            print('no file')
            raise

    def read_user_info_text(self, id_user):
        text=[]
        with codecs.open('users/' + str(id_user) + '/' + str(id_user) + '.txt', 'r', encoding='utf-8') as file:
            for line in file.readlines():
               text.append(line.replace('\n', ''))
            file.close()
            return text

    def read_all_users_text(self):
        file = open('users/users.txt', 'r')
        text = file.readlines()
        file.close()
        return text
