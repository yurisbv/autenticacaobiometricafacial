from dependencies.face_recognizer.Entities.User import User
from dependencies.face_recognizer.MetaClass.MetaSingleton import MetaSingleton

import cv2
import os
import datetime


class FaceDetector(object, metaclass=MetaSingleton):
    _haarCascade = os.getcwd()+'/dependencies/face_recognizer/Dependencies/haarcascade_frontalface_default.xml'
    _maxfaces = 25
    _video = None
    _classifier = None
    _user = User()

    def __init__(self, maxfaces=None):
        self._classifier = cv2.CascadeClassifier(self._haarCascade)
        if maxfaces and isinstance(maxfaces, int):
            self._maxfaces = maxfaces

    def run(self, video, user):
        print('video', str(video))
        print('usuario', str(user))
        image_faces = []
        self._video = cv2.VideoCapture(video)
        self._user = user
        while True:
            image, frame = self._video.read()
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            detected_faces = self._classifier.detectMultiScale(frame_gray, scaleFactor=1.5, minNeighbors=5, minSize=(70,70))

            for x, y, l, a in detected_faces:
                cv2.rectangle(frame, (x, y), (x + l, y + a), (0, 0, 255), 2)
                image_faces.append(frame_gray[y:(y + a), x:(x + l)])

            if len(image_faces) == self._maxfaces or cv2.waitKey(1) == ord('f'):

                for i, face in enumerate(image_faces):
                    cv2.imwrite("users/" + str(user.get_id()) + "/" + str(i) + ".jpg", face)
                break

        self._video.release()
        self.clean_video_register()

    def clean_video_register(video):
        cv2.destroyAllWindows()
