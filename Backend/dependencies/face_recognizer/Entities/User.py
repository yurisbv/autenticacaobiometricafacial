
class User(object):

    def set_name(self, name):
        self._name=name

    def set_id(self, id):
        self._id=id

    def set_create_at(self, create_at):
        self._create_at=create_at

    def set_last_change(self, last_change):
        self._last_change=last_change

    def get_name(self):
        return self._name

    def get_id(self):
        return self._id

    def get_create_at(self):
        return self._create_at

    def get_last_change(self):
        return self._last_change


