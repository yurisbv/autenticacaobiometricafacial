import os
import time
from flask import Flask, request, redirect, flash, render_template
from werkzeug.utils import secure_filename

##Face
from dependencies.face_recognizer.Services.UserManager import UserManager
from dependencies.face_recognizer.Services.FaceDetector import FaceDetector


UPLOAD_FOLDER = os.getcwd() + '/uploads'
ALLOWED_EXTENSIONS = set(['avi'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
date = time.strftime("%d-%m-%Y_%H-%M-%S_")


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/auth/register', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        username = request.form['username']
        userid = request.form['userid']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '' or username == '' or userid == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            #
            filename = date + secure_filename(username) + "." + file.filename.rsplit('.', 1)[1].lower()
            file_dir = os.path.join(app.config['UPLOAD_FOLDER'],  filename)
            print(filename)
            file.save(file_dir)

            ## return if not valid video, or a vídeo from some user!
            ##  return se não for um vídeo válido,
            #  ou se for um vídeo de um usuário pode redirecionar para o login se o vídeo for válido


            ##Create a user in your Data Base, but handle a commit
            ##Crie o usuário em seu banco mas tenha na mãos o commit para o caso de repetição de usuário

            ## Register FaceRecognizer
            #Create User

            ## your database user id, username
            user_manager = UserManager()
            face_detector = FaceDetector(50)
            new_user = user_manager.create_user(userid, username)
            print('new user ', new_user.get_id(), str(new_user.get_name()))
            face_detector.run(file_dir, new_user)


            return redirect('success')
    return render_template('register.html')

@app.route('/success')
def uploaded_file():
    return render_template('success.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)